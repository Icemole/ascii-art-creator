# ASCII art creator

Creates ASCII art from a given image.

Inspired from https://robertheaton.com/2018/06/12/programming-projects-for-advanced-beginners-ascii-art/


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
```bash
make
```

## Usage
```bash
./ascii-art <path-to-img>
```

## Dependencies
```bash
opencv
vtk
hdf5
glew
pugixml
fmt
cxxopts
```

## License
For open source projects, say how it is licensed.