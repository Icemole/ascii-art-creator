#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <cmath>
#include <fmt/color.h>
#include <cxxopts.hpp>


//const std::string BRIGHTNESS_TO_CHAR = "`^\",:;Il!i~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$";
const std::string BRIGHTNESS_TO_CHAR = ".,-~:;=!*#$@";
const int NUM_CHARS = BRIGHTNESS_TO_CHAR.length();
const int MAX_BRIGHTNESS = 255;


using namespace cv;


float compute_brightness(int r, int g, int b) {
    return (float) (r + g + b) / 3.0;
}

float compute_brightness(Vec3b vec) {
    uchar r = vec.val[0];
    uchar g = vec.val[1];
    uchar b = vec.val[2];
    return (float) (r + g + b) / 3.0;
}

char get_char_from_brightness(float brightness) {
    int char_index = std::roundf(brightness / MAX_BRIGHTNESS * NUM_CHARS);
    return BRIGHTNESS_TO_CHAR[char_index];
}

int main(int argc, char *argv[]) {
    cxxopts::Options options("ascii-art-creator",
            "Creates ASCII art from a given image");
    options.add_options()
            ("i,image", "REQUIRED. Image to convert to ASCII", cxxopts::value<std::string>())
            ("d,downsampling_factor", "Downsampling factor, useful to make the output fit in the terminal", cxxopts::value<float>()->default_value("1.0f"))
            ("c,color", "Display colored result", cxxopts::value<bool>()->default_value("false"))
    ;

    // Parse cmdline options
    auto result = options.parse(argc, argv);
    std::string image_path;
    if (result.count("image")) {
        image_path = result["image"].as<std::string>();
    }
    else {
        std::cout << options.help() << std::endl;
        exit(1);
    }
    float downsampling_factor = result["downsampling_factor"].as<float>();
    bool color = result["color"].as<bool>();

    // Get image
    Mat img = imread(image_path);
    // OpenCV loads images in BGR format, change it to RGB for better handling
    cvtColor(img, img, COLOR_BGR2RGB);

    // Downsample image to fit terminal window better
    Mat downsampled_img;
    resize(img, downsampled_img, Size(img.rows / downsampling_factor, img.cols / downsampling_factor));
    
    for (int i = 0; i < downsampled_img.rows; i++) {
        for (int j = 0; j < downsampled_img.cols; j++) {
            float brightness = compute_brightness(downsampled_img.at<Vec3b>(i, j));
            char printable = get_char_from_brightness(brightness);
            if (color) {
                fmt::v8::rgb my_rgb;
                my_rgb.r = downsampled_img.at<Vec3b>(i, j)[0];
                my_rgb.g = downsampled_img.at<Vec3b>(i, j)[1];
                my_rgb.b = downsampled_img.at<Vec3b>(i, j)[2];
                fmt::print(fg(my_rgb), "{}", printable);
            }
            else {
                std::cout << printable;
            }
        }
        std::cout << std::endl;
    }

    return 0;
}